﻿using UnityEditor;

public class CreateAssetBundle
{
    [UnityEditor.MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles() 
    {
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.Android);
    }
}
