﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Recorder;

public class InformationAboutOrganization : MonoBehaviour
{
    public RawImage Image;
    public TextMeshProUGUI OrganizationName;
    public TextMeshProUGUI OrganizationMarkersCount;
    public TextMeshProUGUI OrganizationAddress;
    public InstitutionJsonLoader.InstitutionList Institution;
    public bool isAviable = false;

    private StickerConfirmation _stickerConfirmation;
    private void Start()
    {
        _stickerConfirmation = transform.parent.GetComponent<StickerConfirmation>();
        Button button = transform.GetChild(0).GetComponent<Button>();
        button.onClick.AddListener(() => {
            if (isAviable)
            {
                if (GlobalVariables.institution == null || GlobalVariables.institution.data.id != Institution.data.id)
                {
                    _stickerConfirmation.ShowConfirmationWindow();
                }
                else {
                    OpenStickerMenu();
                }
                _stickerConfirmation.ChangeConfirmationButton(ButtonProcess);
            }
            else {
                ScreenRecorder.ShowToast("Данные маркеры не скачаны");
                Debug.Log("Данные маркеры не скачаны");
            }
        });
    }

    void ButtonProcess()
    {
        //Cмена кэша
        if (GlobalVariables.institution == null || GlobalVariables.institution.data.id != Institution.data.id)
        {
            ChangeInstitution();
        }
        else //открытия меню стикеров
        {
            OpenStickerMenu();
        }
    }
    private void ChangeInstitution() {
        GlobalVariables.institution = Institution;
        GlobalVariables.isInstChanged = true;
        Debug.Log("Заведение заменено");
    }
    private void OpenStickerMenu() {
        MarkerInfo markerInfo = transform.parent.gameObject.GetComponent<MarkerInfo>();
        markerInfo.ScrollView.SetActive(true);
        markerInfo.ContentSticker.GetComponent<StickerImage>().StartLoad(Institution);
        markerInfo.Organizations.SetActive(false);
    }
}
