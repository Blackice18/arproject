﻿using UnityEngine;
using System;

public class TokenClass : MonoBehaviour
{
    [Serializable]
    public class TokenResponse
    {
        public string token_type;
        public int expires_in;
        public string access_token;
        public string refresh_token;
    }
}
