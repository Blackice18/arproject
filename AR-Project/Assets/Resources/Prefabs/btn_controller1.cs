﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class btn_controller1 : MonoBehaviour
{
    public GameObject button;
    public bool isPressed = false;
    void Start()
    {
        
    }
    public void OnMouseDown()
    {
        if (!isPressed) 
        {
            button.GetComponent<Animator>().Play("Press");
            isPressed = true;
        }
        
    }
    public void OnMouseUp()
    {
        button.GetComponent<Animator>().Play("None");
        isPressed = false;
    }

    public void Update()
    {

    }
}
