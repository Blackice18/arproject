﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickerConfirmation : MonoBehaviour
{
    [SerializeField] private GameObject _confirmation;
    [SerializeField] private Button _confirmationButton;

    private void Start()
    {
        Debug.Log(_confirmation);
    }
    public void ShowConfirmationWindow() {_confirmation.SetActive(true);}
    public void HideConfirmationWindow() { _confirmation.SetActive(false);}
    public void ChangeConfirmationButton(Action process)
    {
        _confirmationButton.onClick.AddListener(()=> {
            process();
            HideConfirmationWindow();
        }) ;
    }
}
